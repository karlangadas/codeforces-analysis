# codeforces-analysis

Users Indicators Builder

### Requirements
- Python3.6+
- 'nlogonia.csv': A csv file with codeforces profile links (column name: link)

### Execution
- `python get_problem_stats.py`
- `python get_rating.py`

