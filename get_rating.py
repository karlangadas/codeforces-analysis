import requests
import pandas as pd

df = pd.read_csv('nlogonia.csv')
df['rating'] = 0
df['level'] = ''

lista = []
for i, row in df.iterrows():
    link = 'no'
    handle = 'no'
    rating = 0
    rank = 'no'
    
    if row['link'] != 'no':
        link = row['link']
        handle = row['link'][31:]
        print(handle)
        r = requests.get(f'https://codeforces.com/api/user.info?handles={handle}')
        rating = r.json()['result'][0]['rating']
        rank = r.json()['result'][0]['rank']

    lista.append([link, handle, rating, rank])

result_df = pd.DataFrame(lista,columns = ['link', 'handle', 'rating', 'rank'])
result_df.to_csv('result_df.csv')