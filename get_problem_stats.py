import requests
import pandas as pd

df = pd.read_csv('nlogonia.csv')

dict_list = []
total_tags = set()
for i, row in df.iterrows():
    sub_id_list = []
    elem = {}
    elem['link'] = 'no'
    elem['handle'] = 'no'
    if row['link'] != 'no':
        elem['link'] = row['link']
        elem['handle'] = handle = row['link'][31:]
        r = requests.get(f'https://codeforces.com/api/user.status?handle={handle}&from=1')
        try:
            subs = r.json()['result']
        except:
            print(elem['handle'])
            dict_list.append(elem)
            continue
        #accepted first
        for sub in subs:
            try:
                sub_id = str(sub['contestId']) + sub['problem']['index']
            except:
                sub_id = str(sub['problem']['problemsetName']) + str(sub['problem']['index'])
            if sub_id in sub_id_list:
                continue
            if sub['verdict'] == 'OK':
                sub_id_list.append(sub_id)
                tags = sub['problem']['tags']
                for tag in tags:
                    total_tags.add(tag)
                    if f'{tag}_total' not in elem:
                        elem[f'{tag}_ac'] = 0
                        elem[f'{tag}_total'] = 0
                    elem[f'{tag}_ac'] += 1    
                    elem[f'{tag}_total'] += 1
                    
        #not accepted
        for sub in subs:
            try:
                sub_id = str(sub['contestId']) + sub['problem']['index']
            except:
                sub_id = str(sub['problem']['problemsetName']) + str(sub['problem']['index'])
            if sub_id in sub_id_list:
                continue
            if sub['verdict'] != 'OK':
                sub_id_list.append(sub_id)
                tags = sub['problem']['tags']
                for tag in tags:
                    if f'{tag}_total' not in elem:
                        elem[f'{tag}_total'] = 0  
                    elem[f'{tag}_total'] += 1
            
    dict_list.append(elem)

result_df = pd.DataFrame(dict_list)

total_tags = list(total_tags)

for t in total_tags:
    result_df[f'{t}_%'] = result_df[f'{t}_ac'] / result_df[f'{t}_total']

result_df = result_df.reindex(sorted(result_df.columns), axis=1)
result_df.to_csv('result2_df.csv')