import requests
import pandas as pd

df = pd.read_csv('team.csv')

dict_list = []
for i, row in df.iterrows():
    handle = row['link'][31:]
    r = requests.get(f'https://codeforces.com/api/user.status?handle={handle}')
    try:
        subs = r.json()['result']
    except:
        print(handle)
        continue
    for sub in subs:
        participant_type = sub['author']['participantType']
        verdict = sub['verdict']
        members = sub['author']['members']
        if verdict != 'OK' or len(members) > 1:
            continue
        if not (participant_type == 'CONTESTANT' or participant_type == 'VIRTUAL'):
            continue
        elem = {}
        elem['handle'] = handle
        elem['contest_id'] = sub['contestId']
        elem['problem_idx'] = sub['problem']['index']
        elem['relatime_time'] = sub['relativeTimeSeconds']
        try:
            elem['points'] = sub['problem']['points']
        except:
            elem['points'] = 1600
        tags = sub['problem']['tags']
        for tag in tags:
            elem['tag'] = tag
            dict_list.append(elem.copy())
    
result_df = pd.DataFrame(dict_list)

#Get duration
results_problem_begin = result_df[['handle','contest_id']].drop_duplicates().copy()
results_problem_begin['problem_idx'] = 'Z'
results_problem_begin['relatime_time'] = 0

results_problem = result_df[['handle','contest_id','problem_idx','relatime_time']].drop_duplicates().copy()
results_problem = pd.concat([results_problem_begin, results_problem])
results_problem = results_problem.sort_values(by=['handle','contest_id','relatime_time'])
results_problem['prev_time'] = results_problem['relatime_time'].shift(1)
results_problem['duration'] = results_problem['relatime_time'] - results_problem['prev_time']
results_problem = results_problem[results_problem['problem_idx']!='Z']

result_df = result_df[['handle','contest_id','problem_idx','points','tag']]
result_df = pd.merge(result_df, results_problem, on=['handle','contest_id','problem_idx'])

result_df.to_csv('result6_df.csv')
